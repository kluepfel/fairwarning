//
//  FWLocation.m
//  FairWarning
//
//  Created by Tobias Klüpfel on 25/04/14.
//  Copyright (c) 2014 Tobias Klüpfel. All rights reserved.
//
/*
 {
    base = "cmc stations";
    clouds =     {
        all = 24;
    };
    cod = 200;
    coord =     {
        lat = "47.68";
        lon = "11.57";
    };
    dt = 1398437499;
    id = 6556127;
    main =     {
        "grnd_level" = "869.73";
        humidity = 87;
        pressure = "869.73";
        "sea_level" = "1020.92";
        temp = "11.387";
        "temp_max" = "11.387";
        "temp_min" = "11.387";
    };
    name = Lenggries;
    rain =     {
        3h = 1;
    };
    sys =     {
        country = DE;
        message = "0.0224";
        sunrise = 1398398731;
        sunset = 1398449864;
    };
    weather =     (
                   {
                       description = "light rain";
                       icon = 10d;
                       id = 500;
                       main = Rain;
                   }
                   );
    wind =     {
        deg = "318.503";
        speed = "0.31";
    };
}

 
 
 
 {"cod":"200","message":0.0242,"city":{"id":6556127,"name":"Lenggries","coord":{"lon":11.5667,"lat":47.6833},"country":"DE","population":9771},"cnt":16,"list":[{"dt":1398769200,"temp":{"day":13.6,"min":11.6,"max":13.6,"night":11.76,"eve":13.6,"morn":13.6},"pressure":955.96,"humidity":90,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":1.76,"deg":277,"clouds":92,"rain":5},{"dt":1398855600,"temp":{"day":15.83,"min":8.17,"max":15.83,"night":8.17,"eve":10.76,"morn":11.39},"pressure":953.39,"humidity":94,"weather":[{"id":502,"main":"Rain","description":"heavy intensity rain","icon":"10d"}],"speed":2.33,"deg":36,"clouds":20,"rain":15},{"dt":1398942000,"temp":{"day":10.83,"min":8.7,"max":12.79,"night":9.52,"eve":12.3,"morn":8.7},"pressure":957.39,"humidity":99,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":3.81,"deg":250,"clouds":92,"rain":8},{"dt":1399028400,"temp":{"day":13.04,"min":8.62,"max":13.04,"night":8.62,"eve":10.77,"morn":9.4},"pressure":953.8,"humidity":96,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":1.87,"deg":319,"clouds":88,"rain":8},{"dt":1399114800,"temp":{"day":6.46,"min":3.8,"max":8.5,"night":3.8,"eve":4.98,"morn":8.5},"pressure":958.7,"humidity":100,"weather":[{"id":502,"main":"Rain","description":"heavy intensity rain","icon":"10d"}],"speed":3.76,"deg":353,"clouds":92,"rain":28},{"dt":1399201200,"temp":{"day":7.4,"min":3.8,"max":7.4,"night":4.7,"eve":5.98,"morn":3.8},"pressure":963.26,"humidity":88,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":4.17,"deg":307,"clouds":68,"rain":2},{"dt":1399287600,"temp":{"day":9.3,"min":1.13,"max":9.3,"night":1.13,"eve":8.36,"morn":2.17},"pressure":939.54,"humidity":0,"weather":[{"id":600,"main":"Snow","description":"light snow","icon":"13d"}],"speed":2.21,"deg":297,"clouds":12,"rain":1.13,"snow":0.46},{"dt":1399374000,"temp":{"day":15.81,"min":4.29,"max":15.81,"night":5.35,"eve":12.78,"morn":4.29},"pressure":936.68,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":1.75,"deg":184,"clouds":58,"rain":0.52},{"dt":1399460400,"temp":{"day":15.05,"min":7.59,"max":15.05,"night":9.31,"eve":14.93,"morn":7.59},"pressure":938.86,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":1.58,"deg":150,"clouds":88,"rain":2.4},{"dt":1399546800,"temp":{"day":14.83,"min":6.93,"max":14.83,"night":6.93,"eve":13.75,"morn":10.34},"pressure":941.84,"humidity":0,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":3.79,"deg":270,"clouds":32,"rain":6.16},{"dt":1399633200,"temp":{"day":20.89,"min":9.23,"max":20.89,"night":11.19,"eve":18.14,"morn":9.23},"pressure":937.24,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":2.53,"deg":116,"clouds":38,"rain":2.27},{"dt":1399719600,"temp":{"day":19.84,"min":9.81,"max":19.84,"night":9.81,"eve":17.24,"morn":12.2},"pressure":940.28,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":1.44,"deg":267,"clouds":20,"rain":2.49},{"dt":1399806000,"temp":{"day":22.21,"min":11.8,"max":22.21,"night":11.8,"eve":20.01,"morn":12.84},"pressure":938.7,"humidity":0,"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"speed":1.53,"deg":181,"clouds":0},{"dt":1399892400,"temp":{"day":24.64,"min":14.91,"max":24.64,"night":15.05,"eve":22.75,"morn":14.91},"pressure":936.94,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":1.53,"deg":35,"clouds":0},{"dt":1399978800,"temp":{"day":22.43,"min":13.04,"max":22.43,"night":13.04,"eve":15.26,"morn":15.75},"pressure":935.18,"humidity":0,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":3.48,"deg":331,"clouds":20,"rain":10.85},{"dt":1400065200,"temp":{"day":15.44,"min":9.32,"max":15.44,"night":9.32,"eve":14.73,"morn":10.54},"pressure":941.11,"humidity":0,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":3.45,"deg":93,"clouds":74,"rain":3.47}]}
*/



#import "FWLocation.h"

NSString * const kFWLocationDidUpdateNotification = @"kFWLocationDidUpdateNotification";

@interface FWLocation ()

@end

@implementation FWLocation

- (void)fetchDataFromInternets {
    // 5 minutes
    if (self.lastFetchDate && [self.lastFetchDate timeIntervalSinceNow] > -(60*5)) {
        return;
    }
    
    if (self.identifier == 0) return;
    
    NSURL *currentWeatherURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?id=%d&units=metric&mode=json", self.identifier]];
    NSURLRequest *currentWeatherRequest = [NSURLRequest requestWithURL:currentWeatherURL];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:currentWeatherRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *urlSessionError) {
        if (urlSessionError) {
            NSLog(@"url session error: %@", urlSessionError);
        }
        
        // parse
        NSError *parseError;
        id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        
        self.name = jsonObject[@"name"];
        self.currentTemperature = [jsonObject[@"main"][@"temp"] floatValue];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kFWLocationDidUpdateNotification object:self];
    }] resume];
    
    NSURL *forecastURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?id=%d&mode=json&units=metric&cnt=16", self.identifier]];
    NSURLRequest *forecastRequest = [NSURLRequest requestWithURL:forecastURL];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:forecastRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *urlSessionError) {
        if (urlSessionError) {
            NSLog(@"url session error: %@", urlSessionError);
        }
        
        // parse
        NSError *parseError;
        id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];

        self.forecast = jsonObject[@"list"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kFWLocationDidUpdateNotification object:self];
    }] resume];
    
    self.lastFetchDate = [NSDate date];
}

@end
