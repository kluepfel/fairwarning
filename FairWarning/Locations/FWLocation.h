//
//  FWLocation.h
//  FairWarning
//
//  Created by Tobias Klüpfel on 25/04/14.
//  Copyright (c) 2014 Tobias Klüpfel. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString * const kFWLocationDidUpdateNotification;


@interface FWLocation : NSObject <NSURLConnectionDataDelegate>

@property (nonatomic) int identifier;
@property (nonatomic, strong) NSString* name;
@property (nonatomic) float currentTemperature;
@property (nonatomic, strong) NSDate* lastFetchDate;

@property (nonatomic, strong) NSArray *forecast;

- (void)fetchDataFromInternets;

@end
