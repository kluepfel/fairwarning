//
//  FWDetailViewController.h
//  FairWarning
//
//  Created by Tobias Klüpfel on 25/04/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FWLocation.h"

@interface FWDetailViewController : UIViewController <UITableViewDataSource>

@property (strong, nonatomic) FWLocation *detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
