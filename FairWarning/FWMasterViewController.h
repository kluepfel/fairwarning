//
//  FWMasterViewController.h
//  FairWarning
//
//  Created by Tobias Klüpfel on 25/04/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FWMasterViewController : UITableViewController

@property (nonatomic, weak) IBOutlet UITableView *O_tableView;

@end
