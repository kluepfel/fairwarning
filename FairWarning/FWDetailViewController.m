//
//  FWDetailViewController.m
//  FairWarning
//
//  Created by Tobias Klüpfel on 25/04/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "FWDetailViewController.h"

@interface FWDetailViewController ()
- (void)configureView;

@property (nonatomic, weak) IBOutlet UITableView *O_forecastTableView;
@end

@implementation FWDetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 14;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.O_forecastTableView dequeueReusableCellWithIdentifier:@"forecastCell" forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%d", indexPath.row];
    id rowData = _detailItem.forecast[indexPath.row];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.1f℃ - %.1f℃", [rowData[@"temp"][@"min"] floatValue], [rowData[@"temp"][@"max"]floatValue]];

    return cell;
}

@end
